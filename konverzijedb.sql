-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 16, 2018 at 07:19 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `konverzijedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `konverzije`
--

DROP TABLE IF EXISTS `konverzije`;
CREATE TABLE IF NOT EXISTS `konverzije` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `timestamp` int(15) NOT NULL,
  `iznos` varchar(10) NOT NULL,
  `valuta_kod` varchar(20) NOT NULL,
  `kurs` varchar(10) NOT NULL,
  `vrednost` varchar(10) NOT NULL,
  `total` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `konverzije`
--

INSERT INTO `konverzije` (`id`, `timestamp`, `iznos`, `valuta_kod`, `kurs`, `vrednost`, `total`) VALUES
(37, 1529105244, '1213', 'rub', 'prodajni', '1.6385', '1987.5005'),
(36, 1529103637, '100', 'cad', 'srednji', '77.7839', '7778.39'),
(35, 1529102561, '100', 'gbp', 'srednji', '135.3092', '13530.92'),
(34, 1529102532, '100', 'gbp', 'srednji', '135.3092', '13530.92'),
(38, 1529127934, '5', 'jpy', 'srednji', '0.921005', '4.605025'),
(39, 1529129881, '56', 'chf', 'srednji', '102.3313', '5730.5528'),
(40, 1529129934, '56', 'chf', 'srednji', '102.3313', '5730.5528'),
(41, 1529129977, '56', 'chf', 'srednji', '102.3313', '5730.5528'),
(42, 1529130291, '56', 'chf', 'srednji', '102.3313', '5730.5528'),
(43, 1529130300, '56', 'chf', 'srednji', '102.3313', '5730.5528'),
(44, 1529130422, '56', 'chf', 'srednji', '102.3313', '5730.5528'),
(45, 1529130455, '56', 'chf', 'srednji', '102.3313', '5730.5528'),
(46, 1529130970, '123', 'cad', 'srednji', '77.7839', '9567.4197'),
(47, 1529131035, '123', 'cad', 'srednji', '77.7839', '9567.4197'),
(48, 1529131080, '123', 'cad', 'srednji', '77.7839', '9567.4197'),
(49, 1529131082, '123', 'cad', 'srednji', '77.7839', '9567.4197'),
(50, 1529131117, '123', 'cad', 'srednji', '77.7839', '9567.4197'),
(51, 1529131127, '123', 'cad', 'srednji', '77.7839', '9567.4197'),
(52, 1529131141, '123', 'cad', 'srednji', '77.7839', '9567.4197'),
(53, 1529132414, '199', 'eur', 'kupovni', '117.838', '23449.762'),
(54, 1529133069, '123', 'eur', 'srednji', '118.1926', '14537.6898'),
(55, 1529133220, '125', 'gbp', 'srednji', '135.3092', '16913.65'),
(56, 1529133245, '125', 'gbp', 'srednji', '135.3092', '16913.65'),
(57, 1529133249, '125', 'gbp', 'srednji', '135.3092', '16913.65'),
(58, 1529133275, '125', 'gbp', 'srednji', '135.3092', '16913.65'),
(59, 1529133276, '125', 'gbp', 'srednji', '135.3092', '16913.65'),
(60, 1529133311, '125', 'gbp', 'srednji', '135.3092', '16913.65'),
(61, 1529133332, '125', 'gbp', 'srednji', '135.3092', '16913.65'),
(62, 1529133422, '125', 'gbp', 'srednji', '135.3092', '16913.65'),
(63, 1529133455, '125', 'gbp', 'srednji', '135.3092', '16913.65'),
(64, 1529133496, '125', 'gbp', 'srednji', '135.3092', '16913.65');

-- --------------------------------------------------------

--
-- Table structure for table `kurs_dinara`
--

DROP TABLE IF EXISTS `kurs_dinara`;
CREATE TABLE IF NOT EXISTS `kurs_dinara` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `valuta_id` varchar(6) NOT NULL,
  `kupovni` varchar(10) NOT NULL,
  `srednji` varchar(10) NOT NULL,
  `prodajni` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kurs_dinara`
--

INSERT INTO `kurs_dinara` (`id`, `datum`, `valuta_id`, `kupovni`, `srednji`, `prodajni`) VALUES
(1, '2018-06-14', '2', '117.8123', '118.1668', '118.5213'),
(2, '2018-06-14', '1', '99.7648', '100.065', '100.3652'),
(3, '2018-06-14', '5', '101.2568', '101.5615', '101.8662'),
(4, '2018-06-14', '3', '133.6194', '134.0215', '134.4236'),
(5, '2018-06-14', '4', '75.4578', '75.6849', '75.912'),
(6, '2018-06-14', '6', '76.906', '77.1374', '77.3688'),
(7, '2018-06-14', '0', '11.5908', '11.6257', '11.6606'),
(8, '2018-06-14', '0', '15.8099', '15.8575', '15.9051'),
(9, '2018-06-14', '0', '12.4556', '12.4931', '12.5306'),
(10, '2018-06-14', '0', '0.905691', '0.908416', '0.911141'),
(11, '2018-06-14', '0', '1.593', '1.5978', '1.6026'),
(12, '2018-06-14', '0', '15.5958', '15.6427', '15.6896'),
(13, '2018-06-14', '0', '', '15.9959', ''),
(14, '2018-06-14', '0', '', '331.185', ''),
(15, '2018-06-14', '0', '', '27.6213', ''),
(16, '2018-06-14', '0', '', '4.594', ''),
(17, '2018-06-14', '0', '', '0.368018', ''),
(18, '2018-06-14', '0', '', '60.4177', ''),
(48, '2018-06-15', '0', '15.8679', '15.9156', '15.9633'),
(47, '2018-06-15', '9', '1.6287', '1.6336', '1.6385'),
(46, '2018-06-15', '8', '0.918242', '0.921005', '0.923768'),
(45, '2018-06-15', '0', '12.5105', '12.5481', '12.5857'),
(44, '2018-06-15', '0', '15.8144', '15.862', '15.9096'),
(43, '2018-06-15', '7', '11.6408', '11.6758', '11.7108'),
(42, '2018-06-15', '6', '77.5505', '77.7839', '78.0173'),
(41, '2018-06-15', '4', '76.0393', '76.2681', '76.4969'),
(40, '2018-06-15', '3', '134.9033', '135.3092', '135.7151'),
(39, '2018-06-15', '5', '102.0243', '102.3313', '102.6383'),
(38, '2018-06-15', '1', '101.7951', '102.1014', '102.4077'),
(37, '2018-06-15', '2', '117.838', '118.1926', '118.5472'),
(49, '2018-06-15', '0', '', '15.9996', ''),
(50, '2018-06-15', '0', '', '337.6931', ''),
(51, '2018-06-15', '0', '', '27.5964', ''),
(52, '2018-06-15', '0', '', '4.5902', ''),
(53, '2018-06-15', '0', '', '0.365955', ''),
(54, '2018-06-15', '0', '', '60.4309', '');

-- --------------------------------------------------------

--
-- Table structure for table `valuta`
--

DROP TABLE IF EXISTS `valuta`;
CREATE TABLE IF NOT EXISTS `valuta` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(50) NOT NULL,
  `kod` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `valuta`
--

INSERT INTO `valuta` (`id`, `naziv`, `kod`) VALUES
(1, 'US Dollar', 'usd'),
(2, 'Euro', 'eur'),
(3, 'British Pound', 'gbp'),
(4, 'Australian Dollar', 'aud'),
(5, 'Swiss Franc', 'chf'),
(6, 'Canadian Dollar', 'cad'),
(7, 'Swedish Krona', 'sek'),
(8, 'Japanese Yen', 'jpy'),
(9, 'Russian ruble', 'rub');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
