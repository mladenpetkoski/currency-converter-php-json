
<?php include('inc/db.php'); ?>
<?php include('inc/sqltojson.php'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Mladen Petkoski">
        <meta name="keywords" content="valuta, eur, dol, din, exchange, API">
        <title>Devize u RSD konverzija</title>
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <link href="https://fonts.googleapis.com/css?family=VT323&amp;subset=latin-ext" rel="stylesheet">
    </head>

<body>
  <h1>Konverzija u dinare</h1>
  
  <div id="formaCont">
      <form  action="convert.php" method="POST" enctype="multipart/form-data">
        <label>Iznos</label><br>
        <input type="text" name="iznos" value="" required="required"><br><br>
        <label>Valuta</label><br>
        <select type="text" name="valuta"  value="" required="required">
          <option value="">- Choose -</option>";
          <?php

          $sql = "SELECT * FROM valuta ORDER BY naziv ASC";
          $result = mysqli_query($connection,$sql) or die(mysql_error());

          if (mysqli_num_rows($result)>0) {
            while ($record = mysqli_fetch_array($result,MYSQLI_BOTH))
              echo "<option value=\"$record[id]\">$record[naziv]</option>";
          }

          ?>
        </select><br>
      <input type="radio" name="kurs" value="kupovni" checked> Kupovni<br>
      <input type="radio" name="kurs" value="srednji"> Srednji<br>
      <input type="radio" name="kurs" value="prodajni"> Prodajni
      <br><br>
        <input type="submit" name="convert" id="convert" value="convert">
    </form>
 </div>
</body>   